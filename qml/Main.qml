import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Qt.labs.settings 1.0


MainView {
    id: rootID
    objectName: 'mainView'
    applicationName: 'upiano.andrisk'
    automaticOrientation: false
    visible: true
    width: units.gu(45)
    height: units.gu(75)
    
    Settings
    {
        id: settingsObject
        property alias appSoundVolume: volumeControlButtonInstance.volume
    }
    property int keyWide: height / 8
    property double blackKeyWide: width * 1/4

    Flickable {
        width: parent.width
        height: parent.height
        contentHeight: keyWide*22
        contentY: keyWide*7
        pixelAligned: true
        boundsBehavior: Flickable.StopAtBounds
        id: keyboardFlickable

        Rectangle {
            id: pianoId
            anchors.fill: parent

            MultiPointTouchArea {
                anchors.fill: parent
                onPressed: (points) => {
                    for (var i = 0; i < points.length; i++)
                    {
                        //console.log("pressed " + points[i].x + " " + points[i].y + " item pitch: " + pianoId.childAt(points[i].x, points[i].y).pitch);
                        //pianoId.childAt(points[i].x, points[i].y).volume = settingsObject.appSoundVolume / 100.0
                        pianoId.childAt(points[i].x, points[i].y).playSound()
                    }
                }
            }

            Repeater {
                model: 22
                WhiteKey {
                    y: keyWide * index
                    pitch: whiteKeyNoteByIndex(index)
                    Component.onCompleted: {
                        volumeControlButtonInstance.volumeValueChanged.connect(setNewVolumeValue)
                    }
                }
            }

            //Black keys needs to come after White keys so that childAt() function sees them as being on top of the white keys
            //without the change WhiteKey right after the BlackKey would receive the play signal incorrectly when right half of the black key is pressed
            //The z property does not seem to make a difference for the childAt() function (maybe there exists a better solution?)

            Repeater {
                model: 15

                BlackKey {
                    x: blackKeyWide
                    y: keyWide * blackKeyPositionByIndex(index) - height / 2
                    pitch: blackKeyNoteByIndex(index)
                    Component.onCompleted: {
                        volumeControlButtonInstance.volumeValueChanged.connect(setNewVolumeValue)
                    }
                }
            }
        } //Rectangle pianoID
    } //Flickable

    property int panelButtonSize: keyWide * 0.6

    Rectangle
    {
        id: pitchDownButton
        x: parent.width - panelButtonSize
        y: 0
        width: panelButtonSize
        height: panelButtonSize
        color: "black"
        border.width: 1
        border.color: "slategray"
        Icon {
            anchors.fill: parent
            name: "toolkit_arrow-up"
            color: "gray"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                keyboardFlickable.contentY = Math.max(0, keyboardFlickable.contentY - keyWide * 7)
            }
        }
    }

    LockButton
    {
        id: lockButtonInstance
        anchors.top: pitchDownButton.bottom
        anchors.horizontalCenter: pitchDownButton.horizontalCenter
        width: panelButtonSize
        height: panelButtonSize
        border.width: 1
        border.color: "slategray"
        onLockActivated: keyboardFlickable.interactive = false
        onLockDeactivated: keyboardFlickable.interactive = true
    }



    Rectangle
    {
        id: pitchUpButton
        anchors.top: lockButtonInstance.bottom
        anchors.horizontalCenter: lockButtonInstance.horizontalCenter
        width: panelButtonSize
        height: panelButtonSize
        color: "black"
        border.width: 1
        border.color: "slategray"
        Icon {
            anchors.fill: parent
            name: "toolkit_arrow-down"
            color: "gray"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                keyboardFlickable.contentY =
                        Math.min(keyboardFlickable.contentHeight - keyboardFlickable.height, keyboardFlickable.contentY + keyWide * 7)
            }
        }
    }
    VolumeControlButton {
        id: volumeControlButtonInstance
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: pitchUpButton.horizontalCenter
        width: panelButtonSize
        height: panelButtonSize
    }

    ScrollBar {
        active: true
        interactive: false
        orientation: Qt.Vertical
        anchors.top: pitchDownButton.top
        anchors.bottom: pitchUpButton.bottom
        anchors.right: pitchDownButton.left
        policy: ScrollBar.AlwaysOn
        width: panelButtonSize / 4
        size: keyboardFlickable.visibleArea.heightRatio
        position: keyboardFlickable.visibleArea.yPosition
        background: Rectangle {
            color: "black"
            border.width: 1
            border.color: "slategray"
        }
    }

    function whiteKeyNoteByIndex(id)
    {
        return ["C_", "D_", "E_", "F_", "G_", "A_", "H_",
                "C" , "D" , "E" , "F" , "G" , "A",  "H" ,
                "c" , "d" , "e" , "f" , "g" , "a",  "h" ,
                "c-" ][id]
    }

    function blackKeyNoteByIndex(id)
    {
        return ["Cis_", "Dis_", "Fis_", "Gis_", "Ais_",
                "Cis" , "Dis" , "Fis" , "Gis" , "Ais",
                "cis" , "dis" , "fis" , "gis" , "ais", ][id]
    }

    function blackKeyPositionByIndex(id)
    {
        return [1,   2,  4,  5,  6,
                8,   9, 11, 12, 13,
                15, 16, 18, 19, 20][id]
    }

}
