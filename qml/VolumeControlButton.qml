import QtQuick 2.9
import QtQuick.Controls 2.2
import Lomiri.Components 1.3

Rectangle {
    id: volumeButtonRectangle
    color: "black"
    border.width: 1
    border.color: "slategray"
    property alias volume: volumeSlider.value
    signal volumeValueChanged(var volume)

    Icon {
        anchors.fill: parent
        name: volumeToIconName(volumeSlider.value)
        color: "gray"
        rotation: 90
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            confirmationDialog.open()
        }
        Dialog {
            id: confirmationDialog

            x: parent.x //(parent.width - width) / 2
            y: volumeButtonRectangle.y
            width: parent.width - volumeButtonRectangle.width
            height: volumeButtonRectangle.height
            parent: ApplicationWindow.overlay

            modal: true

            Slider {
                id: volumeSlider
                //from: 0
                //to: 100
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width * 0.9
                onValueChanged: {
                    volumeValueChanged(value);
                }
            }
        }
    }

    function volumeToIconName(volume) {
        if (volume === 0) {
            return "audio-volume-low-zero"
        }
        else if (volume > 0 && volume < 33) {
            return "audio-volume-low"
        }
        else if (volume >= 33 && volume < 66) {
            return "audio-volume-medium"
        }
        else if (volume >= 66) {
            return "audio-volume-high"
        }
    }
}
