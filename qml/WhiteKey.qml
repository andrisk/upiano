import QtQuick 2.9
import QtMultimedia 5.8

Item {
    id: keyItem
    width: parent.width
    height: rectangleID.height
    property alias pitch: rectangleID.key
    signal playSound()
    signal setNewVolumeValue(var newVolume)

    Rectangle {
        id: rectangleID
        width: parent.width
        height: rootID.height / 8
        color: "white"
        border.color: "black"
        property string key: "C"

        SoundEffect {
            id: playKey
            source: "sounds/" + rectangleID.key + ".wav"
            category: "alert"
            Component.onCompleted: {
                keyItem.playSound.connect(play)
                keyItem.setNewVolumeValue.connect(setNewVolume)
            }
            function setNewVolume(newVolume) {
                playKey.volume = QtMultimedia.convertVolume(newVolume / 100.0, QtMultimedia.LogarithmicVolumeScale, QtMultimedia.LinearVolumeScale)
                //Following two lines is a workaround fpr volume being updated when sound is already playing. Seems that reinitalizing
                //sound source resets also the volume, so when the SoundEffect is played, it is played with the correct volume right from start.
                source = ""
                source = "sounds/" + rectangleID.key + ".wav"
            }
        }
    }
}
